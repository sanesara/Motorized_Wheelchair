#include "driverlib.h"
#include "Board.h"
#define TIMER_PERIOD 80
//#define INTERVAL_TIME 32768

//#include <time.h>

int oscillations = 0;
int lastCheck = 0; //1 for blocked, 0 for not
double distance = 0;
double threshold = 200;

//int timerCounter = 0;
//double speed = 0;

void main (void)
{
    //initialTime = time(NULL);
    //startTime = time(NULL);
    
    //Stop Watchdog Timer
    WDT_A_hold(WDT_A_BASE);

    //Set A7 as an input pin.
    //Set appropriate module function
    GPIO_setAsPeripheralModuleFunctionInputPin(
            GPIO_PORT_ADC7,
            GPIO_PIN_ADC7,
            GPIO_FUNCTION_ADC7);

    //Set LED1 as an output pin.
    GPIO_setAsOutputPin(
            GPIO_PORT_LED1,
            GPIO_PIN_LED1);

    //Set LED2 as an output pin.
    GPIO_setAsOutputPin(
            GPIO_PORT_LED2,
            GPIO_PIN_LED2);
            
    //Set output pin for blue LED        
    GPIO_setAsOutputPin(
            GPIO_PORT_P1,
            GPIO_PIN6);
            
    GPIO_setOutputHighOnPin(
        GPIO_PORT_P1,
        GPIO_PIN6);
            
    //Set output pin for green LED        
    GPIO_setAsOutputPin(
            GPIO_PORT_P5,
            GPIO_PIN0);
            
    GPIO_setOutputLowOnPin(
        GPIO_PORT_P5,
        GPIO_PIN0);
            
    //Set output pin for yellow LED        
    GPIO_setAsOutputPin(
            GPIO_PORT_P1,
            GPIO_PIN3);
            
    GPIO_setOutputLowOnPin(
        GPIO_PORT_P1,
        GPIO_PIN3);
            
    //Set output pin for orange LED        
    GPIO_setAsOutputPin(
            GPIO_PORT_P1,
            GPIO_PIN5);
            
    GPIO_setOutputLowOnPin(
        GPIO_PORT_P1,
        GPIO_PIN5);
    
    //Set output pin for red LED  
    GPIO_setAsOutputPin(
            GPIO_PORT_P1,
            GPIO_PIN4);
    
    GPIO_setOutputLowOnPin(
        GPIO_PORT_P1,
        GPIO_PIN4
    );

    
    //Port select XT1
    GPIO_setAsPeripheralModuleFunctionInputPin(
        GPIO_PORT_P4,
        GPIO_PIN1 + GPIO_PIN2,
        GPIO_PRIMARY_MODULE_FUNCTION
        );

    //Set external frequency for XT1
    CS_setExternalClockSource(32768);

    //Select XT1 as the clock source for ACLK with no frequency divider
    CS_initClockSignal(CS_ACLK, CS_XT1CLK_SELECT, CS_CLOCK_DIVIDER_1);

    //Start XT1 with no time out
    CS_turnOnXT1(CS_XT1_DRIVE_0);

	//clear all OSC fault flag
	CS_clearAllOscFlagsWithTimeout(1000);

    /*
     * Disable the GPIO power-on default high-impedance mode to activate
     * previously configured port settings
     */
    PMM_unlockLPM5();
    
    // L0~L26 & L36~L39 pins selected
    LCD_E_setPinAsLCDFunctionEx(LCD_E_BASE, LCD_E_SEGMENT_LINE_0, LCD_E_SEGMENT_LINE_26);
    LCD_E_setPinAsLCDFunctionEx(LCD_E_BASE, LCD_E_SEGMENT_LINE_36, LCD_E_SEGMENT_LINE_39);

    LCD_E_initParam initParams = LCD_E_INIT_PARAM;
    initParams.clockDivider = LCD_E_CLOCKDIVIDER_8;
    initParams.muxRate = LCD_E_4_MUX;
    initParams.segments = LCD_E_SEGMENTS_ENABLED;

    // Init LCD as 4-mux mode
    LCD_E_init(LCD_E_BASE, &initParams);

    // LCD Operation - Mode 3, internal 3.08v, charge pump 256Hz
    LCD_E_setVLCDSource(LCD_E_BASE, LCD_E_INTERNAL_REFERENCE_VOLTAGE, LCD_E_EXTERNAL_SUPPLY_VOLTAGE);
    LCD_E_setVLCDVoltage(LCD_E_BASE, LCD_E_REFERENCE_VOLTAGE_3_08V);

    LCD_E_enableChargePump(LCD_E_BASE);
    LCD_E_setChargePumpFreq(LCD_E_BASE, LCD_E_CHARGEPUMP_FREQ_16);

    // Clear LCD memory
    LCD_E_clearAllMemory(LCD_E_BASE);

    // Configure COMs and SEGs
    // L0, L1, L2, L3: COM pins
    // L0 = COM0, L1 = COM1, L2 = COM2, L3 = COM3
    LCD_E_setPinAsCOM(LCD_E_BASE, LCD_E_SEGMENT_LINE_0, LCD_E_MEMORY_COM0);
    LCD_E_setPinAsCOM(LCD_E_BASE, LCD_E_SEGMENT_LINE_1, LCD_E_MEMORY_COM1);
    LCD_E_setPinAsCOM(LCD_E_BASE, LCD_E_SEGMENT_LINE_2, LCD_E_MEMORY_COM2);
    LCD_E_setPinAsCOM(LCD_E_BASE, LCD_E_SEGMENT_LINE_3, LCD_E_MEMORY_COM3);
    

    //Initialize the ADC Module
    /*
     * Base Address for the ADC Module
     * Use internal ADC bit as sample/hold signal to start conversion
     * USE MODOSC 5MHZ Digital Oscillator as clock source
     * Use default clock divider of 1
     */
    ADC_init(ADC_BASE,
        ADC_SAMPLEHOLDSOURCE_SC,
        ADC_CLOCKSOURCE_ADCOSC,
        ADC_CLOCKDIVIDER_1);

    ADC_enable(ADC_BASE);

    /*
     * Base Address for the ADC Module
     * Sample/hold for 16 clock cycles
     * Do not enable Multiple Sampling
     */
    ADC_setupSamplingTimer(ADC_BASE,
        ADC_CYCLEHOLD_16_CYCLES,
        ADC_MULTIPLESAMPLESDISABLE);

    //Configure Memory Buffer
    /*
     * Base Address for the ADC Module
     * Use input A7
     * Use positive reference of Internally generated Vref
     * Use negative reference of AVss
     */
    ADC_configureMemory(ADC_BASE,
        ADC_INPUT_A7,
        ADC_VREFPOS_INT,
        ADC_VREFNEG_AVSS);

    ADC_clearInterrupt(ADC_BASE,
        ADC_COMPLETED_INTERRUPT);

    //Enable Memory Buffer interrupt
    ADC_enableInterrupt(ADC_BASE,
        ADC_COMPLETED_INTERRUPT);

    //Internal Reference ON
    PMM_enableInternalReference();

    //Configure internal reference
    //If ref voltage no ready, WAIT
    while (PMM_REFGEN_NOTREADY == PMM_getVariableReferenceVoltageStatus());
    
    


#ifdef __MSP430_HAS_TBx__
    // Configure TB0 to provide delay for reference settling ~75us
    Timer_B_initUpModeParam initUpModeParam = {0};
    initUpModeParam.clockSource = TIMER_B_CLOCKSOURCE_SMCLK;
    initUpModeParam.clockSourceDivider = TIMER_B_CLOCKSOURCE_DIVIDER_1;
    initUpModeParam.timerPeriod = TIMER_PERIOD;
    initUpModeParam.timerInterruptEnable_TBIE = TIMER_B_TBIE_INTERRUPT_DISABLE;
    initUpModeParam.captureCompareInterruptEnable_CCR0_CCIE =
            TIMER_B_CCIE_CCR0_INTERRUPT_ENABLE;
    initUpModeParam.timerClear = TIMER_B_DO_CLEAR;
    initUpModeParam.startTimer = true;
    Timer_B_initUpMode(TB0_BASE, &initUpModeParam);
#else
    // Configure TA0 to provide delay for reference settling ~75us
    Timer_A_initUpModeParam initUpModeParam = {0};
    initUpModeParam.clockSource = TIMER_A_CLOCKSOURCE_SMCLK;
    initUpModeParam.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    initUpModeParam.timerPeriod = TIMER_PERIOD;
    initUpModeParam.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_DISABLE;
    initUpModeParam.captureCompareInterruptEnable_CCR0_CCIE =
        TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE;
    initUpModeParam.timerClear = TIMER_A_DO_CLEAR;
    initUpModeParam.startTimer = true;
    Timer_A_initUpMode(TIMER_A0_BASE, &initUpModeParam);
#endif  // __MSP430_HAS_TBx__

    __bis_SR_register(CPUOFF + GIE);           // LPM0, TA0_ISR will force exit
    



    //LCD digits
    const char digit[10] =
    {
        0xFC,       //0
        0x60,       //1
        0xDB,       //2
        0xF3,       //3
        0x67,       //4
        0xB7,       //5
        0xBF,       //6
        0xE4,       //7
        0xFF,       //8
        0xF7,       //9
    };
    
    /*CS_initClockSignal(
    CS_FLLREF,
    CS_REFOCLK_SELECT,
    CS_CLOCK_DIVIDER_1
    );

    //Set SMCLK = REFO
    CS_initClockSignal(
        CS_SMCLK,
        CS_REFOCLK_SELECT,
        CS_CLOCK_DIVIDER_1
        );

    //Initialize RTC
    RTC_init(RTC_BASE,
        INTERVAL_TIME,
        RTC_CLOCKPREDIVIDER_1);

    RTC_clearInterrupt(RTC_BASE,
        RTC_OVERFLOW_INTERRUPT_FLAG);

    //Enable interrupt for RTC overflow
    RTC_enableInterrupt(RTC_BASE,
        RTC_OVERFLOW_INTERRUPT);
        
    //Start RTC Clock with clock source SMCLK
    RTC_start(RTC_BASE, RTC_CLOCKSOURCE_SMCLK);*/


    for (;;)
    {
        //Delay between conversions
        __delay_cycles(5000);

        //Enable and Start the conversion
        //in Single-Channel, Single Conversion Mode
        ADC_startConversion(ADC_BASE,
                ADC_SINGLECHANNEL);
                
        //LPM0, ADC_ISR will force exit
        __bis_SR_register(CPUOFF + GIE);


        // LCD Pin20-Pin21 for '3'
        LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_8, digit[oscillations%10]);
        
        // LCD Pin8-Pin9 for '2'
        LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_6, digit[(oscillations%100)/10]);
        
        // LCD Pin12-Pin13 for '1'
        LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_4, digit[(oscillations%1000)/100]);
        
        
        
        if (distance < 10){
            
            //LCD Pin for decimal between 4th and 5th digits
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_11, 0x01);
            
            // LCD Pin16-Pin17 for '4'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_10, digit[(int)distance%10]);
            
            // LCD Pin4-Pin5 for '5'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_2, digit[(int)(distance*10)%10]);
            
            // LCD Pin36-Pin37 for '6'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_18,  digit[(int)(distance*100)%10]);
            
            
        }else if (distance < 100){
            // LCD Pin16-Pin17 for '4'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_10, digit[(int)distance/10]);
            
            // LCD Pin4-Pin5 for '5'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_2, digit[(int)distance%10]);
            
            // LCD Pin36-Pin37 for '6'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_18,  digit[(int)(distance*10)%10]);
            
            //LCD Pin for decimal between 4th and 5th digits
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_11, 0x00);
            
            //LCD Pin for decimal between 5th and 6th digits
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_3, 0x01);
            
        }else{
            
            // LCD Pin16-Pin17 for '4'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_10, digit[((int)distance)/100]);
            
            // LCD Pin4-Pin5 for '5'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_2,  digit[((int)distance/10)%10]);
            
            // LCD Pin36-Pin37 for '6'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_18, digit[((int)distance)%10]);
            
            //LCD Pin for decimal between 5th and 6th digits
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_3, 0x00);
            
        }
        
        /*speed = 10;
        
        if (speed < 10){
            //LCD Pin for decimal between 5th and 6th digits
            //LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_9, 0x01);
            
            // LCD Pin4-Pin5 for '5'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_2, digit[(int)speed%10]);
            // LCD Pin36-Pin37 for '6'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_18, digit[(int)(speed*10)%10]);
        }else{
            // LCD Pin36-Pin37 for '6'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_18, digit[(int)speed%10]);
            
            // LCD Pin4-Pin5 for '5'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_2, digit[((int)speed%100)/10]);
        }*/
        
        /*
        counterForTime++;
        
        if (counterForTime % 100 == 0){
            endTime = time(NULL);
        }
        
        if( endTime != startTime){
            startTime = endTime;
            
            //end = clock();
            totTime = endTime - initialTime;
            double velocity = distance/totTime;
    
            // LCD Pin36-Pin37 for '6'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_18, digit[(int)(velocity)%10]);
    
            // LCD Pin4-Pin5 for '5'
            LCD_E_setMemory(LCD_E_BASE, LCD_E_MEMORY_BLINKINGMEMORY_2, digit[((int)(velocity)%100)/10]);
        }
        */
        
        
        
        // Turn on LCD
        LCD_E_on(LCD_E_BASE);

        //For debug only
        __no_operation();


    }
    
    // Enter LPM3.5
    PMM_turnOffRegulator();
    __bis_SR_register(LPM4_bits | GIE);
    
    
    
}

//ADC interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(ADC_VECTOR)))
#endif
void ADC_ISR (void)
{
    switch (__even_in_range(ADCIV,12)){
        case  0: break; //No interrupt
        case  2: break; //conversion result overflow
        case  4: break; //conversion time overflow
        case  6: break; //ADCHI
        case  8: break; //ADCLO
        case 10: break; //ADCIN
        case 12:        //ADCIFG0

            //Automatically clears ADCIFG0 by reading memory buffer
            //ADCMEM = A0 > 0.5V?
            if (ADC_getResults(ADC_BASE) < 0x155) {
                //Turn LED1 off
                GPIO_setOutputLowOnPin(
                    GPIO_PORT_LED1,
                    GPIO_PIN_LED1
                );

                //Turn LED2 off
                GPIO_setOutputLowOnPin(
                    GPIO_PORT_LED2,
                    GPIO_PIN_LED2
                );
                
                if (lastCheck == 0){
                    oscillations++;
                    distance += (3.14)*(0.85);
                    lastCheck = 1;
                }
                
                
                if (distance >= threshold/4 && distance < threshold/2){
                    GPIO_setOutputLowOnPin(
                        GPIO_PORT_P1,
                        GPIO_PIN6
                    );
                    
                    GPIO_setOutputHighOnPin(
                        GPIO_PORT_P5,
                        GPIO_PIN0
                    );
                }
                else if (distance >= threshold/2 && distance < threshold*3/4){
                    GPIO_setOutputLowOnPin(
                        GPIO_PORT_P5,
                        GPIO_PIN0
                    );
                    
                    GPIO_setOutputHighOnPin(
                        GPIO_PORT_P1,
                        GPIO_PIN3
                    );
                }
                
                else if (distance >= threshold*3/4 && distance < threshold){
                    GPIO_setOutputLowOnPin(
                        GPIO_PORT_P1,
                        GPIO_PIN3
                    );
                    
                    GPIO_setOutputHighOnPin(
                        GPIO_PORT_P1,
                        GPIO_PIN5
                    );
                }
                
                else if (distance >= threshold){
                    GPIO_setOutputLowOnPin(
                        GPIO_PORT_P1,
                        GPIO_PIN5
                    );
                    
                    GPIO_setOutputHighOnPin(
                        GPIO_PORT_P1,
                        GPIO_PIN4
                    );
                }
                
            }
            else {
                //Turn LED1 on
                GPIO_setOutputHighOnPin(
                    GPIO_PORT_LED1,
                    GPIO_PIN_LED1
                );

                //Turn LED2 on
                GPIO_setOutputHighOnPin(
                    GPIO_PORT_LED2,
                    GPIO_PIN_LED2
                );
                
                lastCheck = 0;
            }

            //Clear CPUOFF bit from 0(SR)
            //Breakpoint here and watch ADC_Result
            __bic_SR_register_on_exit(CPUOFF);
            break;
        default: break;
    }
}

#ifdef __MSP430_HAS_TBx__
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_B0_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(TIMER0_B0_VECTOR)))
#endif
void TB0_ISR (void)
{
      TB0CTL = 0;
      LPM0_EXIT;                                // Exit LPM0 on return
}
#else
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_A0_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(TIMER0_A0_VECTOR)))
#endif
void TA0_ISR (void)
{
      TA0CTL = 0;
      LPM0_EXIT;                                // Exit LPM0 on return
}
#endif // __MSP430_HAS_TBx__
